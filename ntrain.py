import argparse
import itertools
import logging
import os
import sys

import torch
from torch import nn
from torch.optim.lr_scheduler import CosineAnnealingLR, MultiStepLR
from torch.utils.data import DataLoader, ConcatDataset

from vision.datasets.voc_dataset import VOCDataset
from vision.nn.multibox_loss import MultiboxLoss
from vision.ssd.config.fd_config import define_img_size
from vision.utils.misc import str2bool, Timer, freeze_net_layers, store_labels
from vision.ssd.config import fd_config
from vision.ssd.data_preprocessing import TrainAugmentation, TestTransform
from vision.ssd.mb_tiny_RFB_fd import create_Mb_Tiny_RFB_fd
from vision.ssd.mb_tiny_fd import create_mb_tiny_fd
from vision.ssd.ssd import MatchPrior



input_img_size =320
net='slim'
num_epochs=200
power=2
overlap_threshold=0.35
datasets=[r'C:\Users\Lenovo\Desktop\dataset']
validation_dataset=r'C:\Users\Lenovo\Desktop\dataset'
checkpoint_folder='models/'
dataset_type ='voc'
num_workers=4
batch_size=24
freeze_net=True
freeze_base_net=True
lr=1e-2
base_net=False
pretrained_ssd=True
momentum=0.9
use_cuda=False
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() and use_cuda else "cpu")
optimizer_type='SGD' #SGD,Adam
scheduler='multi-step'#multi-step,cosine,poly
milestones="80,100"
weight_decay=5e-4
t_max=120
debug_steps=100
validation_epochs=5



define_img_size(input_img_size)

def lr_poly(base_lr, iter):
    return base_lr * ((1 - float(iter) / num_epochs) ** (power))


def adjust_learning_rate(optimizer, i_iter):
    """Sets the learning rate to the initial LR divided by 5 at 60th, 120th and 160th epochs"""
    lr = lr_poly(1e-2, i_iter)
    optimizer.param_groups[0]['lr'] = lr


def train(loader, net, criterion, optimizer, device, debug_steps=100, epoch=-1):
    net.train(True)
    running_loss = 0.0
    running_regression_loss = 0.0
    running_classification_loss = 0.0
    for i, data in enumerate(loader):
        print(".", end="", flush=True)
        images, boxes, labels = data
        images = images.to(device)
        boxes = boxes.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()
        confidence, locations = net(images)
        regression_loss, classification_loss = criterion(confidence, locations, labels, boxes)  # TODO CHANGE BOXES
        loss = regression_loss + classification_loss
        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        running_regression_loss += regression_loss.item()
        running_classification_loss += classification_loss.item()
        if i and i % debug_steps == 0:
            print(".", flush=True)
            avg_loss = running_loss / debug_steps
            avg_reg_loss = running_regression_loss / debug_steps
            avg_clf_loss = running_classification_loss / debug_steps

            print([avg_loss,avg_reg_loss,avg_clf_loss])
            running_loss = 0.0
            running_regression_loss = 0.0
            running_classification_loss = 0.0

def test(loader, net, criterion, device):
    net.eval()
    running_loss = 0.0
    running_regression_loss = 0.0
    running_classification_loss = 0.0
    num = 0
    for _, data in enumerate(loader):
        images, boxes, labels = data
        images = images.to(device)
        boxes = boxes.to(device)
        labels = labels.to(device)
        num += 1

        with torch.no_grad():
            confidence, locations = net(images)
            regression_loss, classification_loss = criterion(confidence, locations, labels, boxes)
            loss = regression_loss + classification_loss

        running_loss += loss.item()
        running_regression_loss += regression_loss.item()
        running_classification_loss += classification_loss.item()
    return running_loss / num, running_regression_loss / num, running_classification_loss / num


if __name__ == '__main__':
    timer = Timer()

    if net == 'slim':
        create_net = create_mb_tiny_fd
        config = fd_config
    elif net == 'RFB':
        create_net = create_Mb_Tiny_RFB_fd
        config = fd_config
    else:
        print("The net type is wrong.")
        sys.exit(1)

    train_transform = TrainAugmentation(config.image_size, config.image_mean, config.image_std)
    target_transform = MatchPrior(config.priors, config.center_variance,
                                  config.size_variance, overlap_threshold)

    test_transform = TestTransform(config.image_size, config.image_mean_test, config.image_std)


    if not os.path.exists('models/'):
        os.makedirs('models/')

    print("Prepare training datasets.")
    # 'dataset' name of varialbe of dataset
    for dataset_path in datasets:
        if dataset_type == 'voc':
            dataset = VOCDataset(dataset_path, transform=train_transform,
                                 target_transform=target_transform)
            label_file = os.path.join(checkpoint_folder, "voc-model-labels.txt")
            store_labels(label_file, dataset.class_names)
            num_classes = len(dataset.class_names)

        else:
            raise print('Dataset type invalid')
        train_loader = DataLoader(dataset, batch_size,num_workers=num_workers,shuffle=True)

    print("Prepare Validation datasets.")
    if dataset_type == "voc":
        val_dataset = VOCDataset(validation_dataset, transform=test_transform,
                                         target_transform=target_transform, is_test=True)

        val_loader = DataLoader(val_dataset, batch_size,
                                    num_workers=num_workers,
                                    shuffle=False)
    net = create_net(num_classes)



    min_loss = -10000.0
    last_epoch = -1

    #used same learning rate for base and extra layers
    base_net_lr = lr
    extra_layers_lr = lr

    if freeze_base_net:
        print("Freeze base net.")
        freeze_net_layers(net.base_net)
        params = itertools.chain(net.source_layer_add_ons.parameters(), net.extras.parameters(),
                                     net.regression_headers.parameters(), net.classification_headers.parameters())
        params = [
                {'params': itertools.chain(
                    net.source_layer_add_ons.parameters(),
                    net.extras.parameters()
                ), 'lr': extra_layers_lr},
                {'params': itertools.chain(
                    net.regression_headers.parameters(),
                    net.classification_headers.parameters()
                )}
            ]
    elif freeze_net:
        freeze_net_layers(net.base_net)
        freeze_net_layers(net.source_layer_add_ons)
        freeze_net_layers(net.extras)
        params = itertools.chain(net.regression_headers.parameters(), net.classification_headers.parameters())
        print("Freeze all the layers except prediction heads.")
    else:
        params = [
                {'params': net.module.base_net.parameters(), 'lr': base_net_lr},
                {'params': itertools.chain(
                    net.module.source_layer_add_ons.parameters(),
                    net.module.extras.parameters()
                ), 'lr': extra_layers_lr},
                {'params': itertools.chain(
                    net.module.regression_headers.parameters(),
                    net.module.classification_headers.parameters()
                )}
            ]

    timer.start("Load Model")

    if base_net:
        net.init_from_base_net(base_net)
    elif pretrained_ssd:
        net.init_from_pretrained_ssd(pretrained_ssd)
    logging.info(f'Took {timer.end("Load Model"):.2f} seconds to load the model.')

    net.to(DEVICE)


    criterion = MultiboxLoss(config.priors, neg_pos_ratio=3,
                             center_variance=0.1, size_variance=0.2, device=DEVICE)
    if optimizer_type == "SGD":
        optimizer = torch.optim.SGD(params, lr=lr, momentum=momentum,
                                    weight_decay=weight_decay)
    elif optimizer_type == "Adam":
        optimizer = torch.optim.Adam(params, lr=lr)
    else:
        print('Unsupported optimizer')
        sys.exit(1)

    if optimizer_type != "Adam":
        if scheduler == 'multi-step':
            milestones = [int(v.strip()) for v in milestones.split(",")]
            scheduler = MultiStepLR(optimizer, milestones=milestones,
                                    gamma=0.1, last_epoch=last_epoch)
        elif scheduler == 'cosine':
            scheduler = CosineAnnealingLR(optimizer, t_max, last_epoch=last_epoch)
        elif scheduler == 'poly':
            print('Uses PolyLR scheduler.')
        else:
            print("Unsupported Scheduler")
            sys.exit(1)

    #training

    for epoch in range(last_epoch + 1, num_epochs):
        if optimizer_type != "Adam":
            if scheduler != "poly":
                if epoch != 0:
                    scheduler.step()
        train(train_loader, net, criterion, optimizer,
              device=DEVICE, debug_steps=debug_steps, epoch=epoch)
        if scheduler == "poly":
            adjust_learning_rate(optimizer, epoch)

        if epoch % validation_epochs == 0 or epoch == num_epochs - 1:
            val_loss, val_regression_loss, val_classification_loss = test(val_loader, net, criterion, DEVICE)

            model_path = os.path.join(checkpoint_folder, f"{net}-Epoch-{epoch}-Loss-{val_loss}.pth")
            net.module.save(model_path)
            logging.info(f"Saved model {model_path}")

